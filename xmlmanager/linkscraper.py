import json


class LinkScraper:

    """ Description

    Scraps the links from the send
    them to LinkDownloader class

    """

    def __init__(self, driver):

        """ Description

        Initialize constants for program


        :type self:
        :param self:

        :type driver:
        :param driver:

        :raises:

        :rtype:
        """
        self.driver = driver
        self.constants_dict = self.get_constants()

    def get_constants(self):

        """ Description
        Get constants from constants.json

        :type self:
        :param self:

        :raises:

        :rtype:
        """
        with open("constants.json", "r") as f:
            constants_dict = json.load(f)
        return constants_dict

    def get_next_page_link(self):

        """ Description
        Driver will get next page link from current page

        :type self:
        :param self:

        :raises:

        :rtype:
        """
        next_page_link = []
        try:
            next_page_link = self.driver.find_element_by_xpath(
                self.constants_dict["XPATH_NEXT_PAGE"]
            )
        except Exception as link_exception:
            print(link_exception)
        return next_page_link

    def get_links(self):

        """ Description
        Get all the links of xmls from the current webpage

        :type self:
        :param self:

        :raises:

        :rtype:
        """

        links_elements = self.driver.find_elements_by_xpath(
            self.constants_dict["XPATH_LINK_ELEMENTS"]
        )
        self.get_next_page_link()
        single_page_links = [
            link.get_attribute("href") for link in links_elements
        ]
        return single_page_links

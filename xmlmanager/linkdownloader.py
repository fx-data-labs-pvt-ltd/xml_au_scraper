import urllib.request
import os


class LinkDownloader:

    """
    Description

    Downloads the links into xmls directory.

    """

    def get_xmls_from_links(self, links):

        """ Description
        This function will download xmls from
        given links

        :type self:
        :param self:

        :type links:
        :param links:

        :raises:

        :rtype:
        """
        print("{} links found".format(len(links)))
        for link in links:
            response = urllib.request.urlopen(link)
            xml_url = response.geturl()
            if not os.path.isdir("xmls"):
                os.mkdir("xmls")
            xml_save_path = os.path.join(
                "xmls",
                urllib.parse.unquote(xml_url).split("/")[-2].split(";")[0],
            )
            if not os.path.exists(xml_save_path):
                urllib.request.urlretrieve(xml_url, xml_save_path)
            else:
                print("Already Saved {}".format(xml_save_path))
        print("All URL retrived. \n Getting next page..")

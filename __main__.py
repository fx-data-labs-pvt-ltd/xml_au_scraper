"""
Description

This is high level script to manage console
level interface.

"""
from xmlscraper import XMLScraper

# High level Script
MAIN_OBJ = XMLScraper()
scrap_flag = input(
    "1. scrape \
    2. Upload on S3 \
    3. Exit\n \
    press any number \n"
)
if scrap_flag == "1":
    MAIN_OBJ.get_xmls()
elif scrap_flag == "2":
    bucket_name = input("Paste Bucket name here: \n")
    # pass debug=True if you want dont want to upload files
    MAIN_OBJ.upload_on_s3(bucket_name, debug=False)

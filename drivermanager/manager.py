import gc
import time

from selenium import webdriver


class DriverManager:

    """ Description
    Handle the selenium driver

    """

    options = webdriver.FirefoxOptions()
    options.headless = True

    @classmethod
    def get_driver(cls):

        """ Description
        Return the Firefox webdriver

        :type cls:
        :param cls:

        :raises:

        :rtype:
        """

        return webdriver.Firefox(
            executable_path=r"geckodriver", firefox_options=cls.options
        )

    @classmethod
    def quit_driver(cls, driver):

        """ Description
        Exit Firefox webdriver

        :type cls:
        :param cls:

        :type driver:
        :param driver:

        :raises:

        :rtype:
        """
        driver.close()
        driver.quit()
        gc.collect()
        time.sleep(2)
        print("Quited Driver", "\n", "*" * 40)

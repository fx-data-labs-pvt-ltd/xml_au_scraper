import logging

import boto3
from botocore.exceptions import ClientError


class CloudUploader:

    """
    Description

    This class will Upload the xmls on AWS S3 bucket.

    """

    def __init__(self, bucket_name, debug=True):

        """ Description

        Initialize the boto3 client for perform
        the operations of uploading xmls

        :type self:
        :param self:

        :type bucket_name:
        :param bucket_name:

        :type debug:
        :param debug:

        :raises:

        :rtype:
        """

        self.bucket_name = bucket_name
        self.debug = debug
        self.s3_client = boto3.client("s3")

    def upload_file(self, file_name, object_name=None):

        """ Description

        Upload file on S3 bucket

        :type self:
        :param self:

        :type file_name:
        :param file_name:

        :type object_name:
        :param object_name:

        :raises:

        :rtype:
        """

        if object_name is None:
            object_name = file_name
        try:
            if not self.debug:
                self.s3_client.upload_file(
                    file_name, self.bucket_name, object_name
                )
        except ClientError as client_error:
            print(client_error)
            logging.error(client_error)
            return False
        return True

    def upload_xml_to_bucket(self, file_name):

        """ Description


        :type self:
        :param self:

        :type file_name:
        :param file_name:

        :raises:

        :rtype:
        """

        logging.basicConfig(
            filename="logs/cloud.log",
            format="%(levelname)s: %(asctime)s: %(message)s",
            filemode="w",
        )
        response = self.upload_file(file_name)
        if response:
            logging.info("File was uploaded")

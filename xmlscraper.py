import json
import logging
import os

from tqdm import tqdm

from cloudmanager.clouduploader import CloudUploader
from drivermanager.manager import DriverManager
from xmlmanager.linkdownloader import LinkDownloader
from xmlmanager.linkscraper import LinkScraper


class XMLScraper:

    """
    Description

    This class is used to download the XML
    from the given website and upload those
    XML into a given S3 bucket.

    """

    def __init__(self):

        """ Description
        Get Drivers for scraping process
        Make object of LinkScraper and LinkDownloader class

        :type self:
        :param self:

        :raises:

        :rtype:
        """

        self.constants_dict = self.get_constants_dict()
        self.driver = DriverManager.get_driver()
        self.obj_link_scrapper = LinkScraper(self.driver)
        self.obj_link_downloader = LinkDownloader()

    def get_constants_dict(self):

        """ Description
        Load constant from constant json file

        :type self:
        :param self:

        :raises:

        :rtype:
        """
        with open("constants.json", "r") as f:
            constants_dict = json.load(f)
        return constants_dict

    def start_scraping(self, init_link):

        """ Description

        This function will start the scraping

        :type self:
        :param self:

        :type init_link:
        :param init_link:

        :raises:

        :rtype:
        """
        self.driver.get(init_link)
        next_page_link = self.obj_link_scrapper.get_next_page_link()
        while next_page_link is not None:
            links = self.obj_link_scrapper.get_links()
            next_page_link = self.obj_link_scrapper.get_next_page_link()
            self.obj_link_downloader.get_xmls_from_links(links)
            next_page_link.click()
            print("--" * 50)

    def get_xmls(self):

        """ Description
        Start the scraping and store the xmls into xmls dir

        :type self:
        :param self:

        :raises:

        :rtype:
        """
        logging.basicConfig(
            filename="logs/scraping.log",
            format="%(asctime)s %(message)s",
            filemode="w",
        )
        init_link = self.constants_dict["init_link"]
        print("Scrapping started from {}".format(init_link))
        try:
            self.start_scraping(init_link)

        except Exception as e:
            logging.error(e)
            print(e)

        finally:
            self.quit_driver()

    def upload_on_s3(self, bucket_name, debug):

        """ Description
        Upload the xmls into AWS S3 bucket

        :type self:
        :param self:

        :type bucket_name:
        :param bucket_name:

        :type debug:
        :param debug:

        :raises:

        :rtype:
        """

        obj_cloud_uploader = CloudUploader(bucket_name, debug=debug)
        if os.path.isdir("xmls"):
            print(
                "Uploading on xml folder in bucket named {}".format(
                    bucket_name
                )
            )
            for xml in tqdm(os.listdir("xmls")):
                obj_cloud_uploader.upload_xml_to_bucket(
                    file_name=os.path.join("xmls", xml)
                )
        else:
            print("Please Scrape Some XMLs first..")

    def quit_driver(self):

        """ Description

        Close the Selenium webdriver

        :type self:
        :param self:

        :raises:

        :rtype:
        """
        DriverManager.quit_driver(self.driver)

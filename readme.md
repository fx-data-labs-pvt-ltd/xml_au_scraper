# Documentation

## Prerequisite:

1. Install Firefox browser for Linux.
2. Download geckodriver for windows/linux from here.
3. Put geckodriver in drivermanager folder.
4. Download python3 from here and install it on your system.
5. Follow this link for guidlines to install virtual environment and pip for installing  packages which our scraper is using.
6. Open folder SCRAPER_AU_XML .
7. Make new environment in this folder by running command virtualenv env -p python3
8. activate environment by run 
    ```
    source env/bin/activate 
    ```
9. After creating virtual environment run command 
    ```
    python3 -m pip install -r requirements.txt
    ```

## Setup Scraper:

1. Open and edit constants.json file and change init_link to the link from which you want to start xml scraping. i.e. “init_link”: “https://www.aph.gov.au/Parliamentary_Business/Hansard?wc=13/10/2015”
2. Scraping will done from link given in init link to most recent month.
3. For AWS credentials you have to put your keys in folder named cloudmanager/.aws/credentials
4. Now set that credentials file for environment by runnig command from folder scrapper_au_xml  
    ```
    export AWS_CONFIG_FILE= “Your aws credentials file path”
    ```


## Start Scraping:

1.  To start script run main.py file by type in cmd/terminal
    ```
    python3 __main__.py 
    ```
2. Follow guidlines in console. 
3. Upload on S3 function will upload all the xmls in the xmls folder. 
4. If any inturruption comes while scraping xmls and script is terminated than simply rerun the command in step 1.
